<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;

class GuestUser extends Controller
{
    public function viewOpenTicket(){
    	return view('/Customer/OpenTicket');
    }

    public function viewOpenTicketForm(){
    	return view('/Customer/OpenTicketForm');
    }

    public function openTicketFormStore(Request $request){

    	 $customer = new Customer;

        $customer->name                 = $request->name;
        $customer->email                = $request->email;
        $customer->phone_number         = $request->phone_number;
        $customer->problem_description  = $request->problem_description;

        $customer->save();
    }
}
