<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\SupportAgent;

class SupportAgentController extends Controller
{
     public function authenticate(Request $request)
    {   
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);


        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('/SupportAgent/agentDashboard/');
           //return redirect('/SupportAgent/agentDashboard/');
        }
    }


    public function showLogin(){
    	return view('/SupportAgent/login');
    }

    public function showDashBoard(){
        return view('/SupportAgent/agentDashboard');
    }
}
