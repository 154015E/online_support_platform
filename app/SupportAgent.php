<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/*class SupportAgent extends Model implements Authenticatable*/
class SupportAgent extends Authenticatable
{    
     //protected $table = 'support_agents';


     protected $fillable = ['name','email','password'];

     protected $hidden = [
    'password', 'remember_token',
     ];
}
