<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;

class SupportAgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   

    public function run()
    {
        DB::table('support_agents')->insert([
           /* 'name' => Str::random(10),
            'email' => Str::random(10).'@gmail.com',
            'password' => Hash::make('password'),*/

            'name'  => 'mihiran',
            'email' => 'mihiran.chathuranga@gmail.com',
            'password' => '123456',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
             ]);
    }
}


