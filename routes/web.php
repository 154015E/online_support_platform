<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Route::get('/Customer/OpenTicket/', 'InitialTicketController@ShowTicketView');

Route::get('/Customer/OpenTicket/', [
    'uses' => 'GuestUser@viewOpenTicket'
  ]);

Route::get('/Customer/OpenTicketForm/', [
    'uses' => 'GuestUser@viewOpenTicketForm'
  ]);

Route::post('/', [
    'uses' => 'GuestUser@openTicketFormStore'
  ]);

Route::get('/SupportAgent/login', [
    'uses' => 'SupportAgentController@showLogin'
  ]);

Route::post('/SupportAgent/agentDashboard/', [
    'uses' => 'SupportAgentController@authenticate'
  ]);

/*Route::get('/SupportAgent/agentDashboard/', [
    'uses' => 'SupportAgentController@showDashBoard'
  ]);*/





