<!doctype html>
<html>
<head>
    <title>Welcome!</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<style>

.topnav {
  overflow: hidden;
  background-color: #e9e9e9;
}



/* Style the input container */
.topnav .login-container {
  float: right;
}



/* Style the button inside the input container */
.topnav .login-container button {
  float: right;
  padding: 6px;
  margin-top: 8px;
  margin-right: 16px;
  background: #ddd;
  font-size: 17px;
  border: none;
  cursor: pointer;
}

.topnav .login-container button:hover {
  background: #ccc;
}

/* Add responsiveness - On small screens, display the navbar vertically instead of horizontally */
@media screen and (max-width: 600px) {
  .topnav .login-container {
    float: none;
  }
   .topnav .login-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;
  }
}
/*-----------------*/

.container {
    width: 100%;
    margin:0px !important;
    padding: 0px !important;
    max-width: unset;
}
.row {
    display: -ms-flexbox;
    display: -webkit-box;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 0px !important;
    margin-left: 0px !important;
}

.background {
    width: 100%;
    height: 100vh;
    background-color: rgb(228, 202, 218);
    background-size: cover;
    color: rgb(67, 67, 67);
    text-align: center;
 }

 .background2 {
    width: 100%;
    height: 100vh;
    background-color: rgb(227, 240, 215);
    background-size: cover;
    color: rgb(67, 67, 67);
    text-align: center;
 }
 .p-style{
    font-size: 20px;
    color:white;
    font-weight: bold;
    margin:auto;
 }

 /*--------------------------------------------*/

 .fill {
    display: flex;
    justify-content: center;
    align-items: center;
    overflow: hidden
}
.fill img {
    flex-shrink: 0;
    min-width: 100%;
    min-height: 100%
}

/*-------------------------------*/
</style>
</head>
<body>
 <div class="topnav">  
    <div class="login-container">
      <form method="get" action="{{ action('SupportAgentController@showLogin') }}">     
        <button type="submit">Login</button>
      </form>
    </div>
    <div class="login-container">
      <form method="get" action="{{ action('GuestUser@viewOpenTicket') }}">     
        <button type="submit">GuestUser</button>
      </form>
    </div>
</div> 


<div class="container">
  <div class="row">
    <div class="col-6 col-lg-6 background">
        <div class="fill">
         <img src="https://www.ontheclock.com/cmsImages/ontheclock-customer-support.jpg" alt="customer_care_girl" width="500" height="600">
        </div> 
    </div>
    <div class="col-6 col-lg-6 background2">

    <div class="card" style="width: 18rem;">
     
        <div class="card-body">
          <h5 class="card-title">Connect with us.</h5>
            <p class="card-text">We are here to support you</p>
            
             
     </div>
    </div>
  </div>
</div>



</body>
</html>