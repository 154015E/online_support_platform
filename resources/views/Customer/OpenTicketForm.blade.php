 <!DOCTYPE html>
<html>
<head>
<title>Support Ticket</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>

<form method="post" action="{{ action('GuestUser@openTicketFormStore') }}">
 {{ csrf_field() }} 
  <div class="form-group">
    <label for="customerName">Customer Name</label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="name" >
   
  </div>
  <div class="form-group">
    <label for="problemDescription">Problem Description</label>
    <input type="text" class="form-control" id="exampleInputPassword1" name="problem_description" >
  </div>

  <div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" id="exampleInputPassword1" name="email" >
  </div>

  <div class="form-group">
    <label for="exampleInputPassword1">Phone Number</label>
    <input type="text" class="form-control" id="exampleInputPassword1" name="phone_number" >
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</body>
</html> 